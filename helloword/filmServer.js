
// **************************************        HDO MOBILE         ********************************//

var Parse = require("parse/node").Parse;
var request = require('request');
var cheerio = require('cheerio');

var listFilmUpdate = []
var listNewBanner = []
var listFilmHot = []
var listFilmSuggestion = []
var listMovieNew = []
var listDrameNew = []
var listCinemaNew = []


// FILM --> uuid , name , nameEng , des , director , imdb , screenShots , sugestions , trailer , timer , type , url , year , poster 

function getFilmDetailsAndSubmitToServer(url,_poster,done){
    request({'url':'http://hdonline.vn' + url,'proxy':'http://112.213.94.36:3128'}, function (error, response, html) {
        if (!error && response.statusCode == 200) {
            var document = cheerio.load(html);
            
            
            var filmUUID = getUUIDFormURL(url);
            var filmName = ''
            var filmEngName = ''
            var filmType = []
            var filmcountry = []
            var filmTime = ''
            var filmYear = ''
            var filmDriector = []
            var filmIMDBScore = '0'
            var filmDescription = ''
            var filmTrailer = ''
            var filmSugestionsFilm = []

            


            document('ul.filminfo-fields').each(function(i,ele){
                var listInfo = document(this).children();
                for(var j=0;j<listInfo.length;j++){
                    var litag = listInfo.eq(j);
                    
                    //console.log(litag.children().eq(0).attr());
                    var LI_content = litag.text();

                    if(LI_content.indexOf('Tên Tiếng Anh:') > -1){
                        filmEngName = LI_content.replace("Tên Tiếng Anh: ","").replace(/^\s+|\s+$/g, '');
                    }
                    else if(LI_content.indexOf('Tên Phim:') > -1){
                        filmName = LI_content.replace("Tên Phim: ","").replace(/^\s+|\s+$/g, '');
                    }
                    else if(LI_content.indexOf('Thể loại:') > -1){
                        for(var k = 0 ; k < litag.children().eq(0).children().length ; k ++){
                            filmType.push(litag.children().eq(0).children().eq(k).text().replace(/^\s+|\s+$/g, ''));
                        }
                    }
                    else if(LI_content.indexOf('Quốc gia: ') > -1){
                        for(var k = 0 ; k < litag.children().eq(0).children().length ; k ++){
                            filmcountry.push(litag.children().eq(0).children().eq(k).text().replace(/^\s+|\s+$/g, ''));
                        }
                    }
                    else if(LI_content.indexOf('Thời lượng:') > -1){
                        filmTime = LI_content.replace('Thời lượng:','').replace(/^\s+|\s+$/g, '');
                    }
                    else if(LI_content.indexOf('Năm sản xuất:') > -1){
                        filmYear = LI_content.replace('Năm sản xuất:','').replace(/^\s+|\s+$/g, '');
                    }
                    else if(LI_content.indexOf('Đạo diễn:') > -1){
                        for(var k = 0 ; k < litag.children().length ; k ++){
                            filmDriector.push(litag.children().eq(k).text().replace(/^\s+|\s+$/g, ''));
                        }
                    }
                    else if(j == listInfo.length - 1){
                        filmIMDBScore = litag.text().replace(/^\s+|\s+$/g, '');
                    }
                    //
                }
                if(filmName == ''){
                    document('div.tn-lockup-title').each(function(i,e){
                        filmName = document(this).text().replace(/^\s+|\s+$/g, '');
                    });
                }


            });
            // film des
            document('div.rating').each(function(i,e){
                filmDescription = document(this).next().children().eq(0).text();
            });
            // trailer
            document('div.header-block-title ').each(function(i,e){
                var urlTrailer = document(this).next().attr('src');
                if(urlTrailer != undefined){
                    //EFnZJ1KbhvU
                    var trailerTemp = urlTrailer.slice(-11);
                    filmTrailer = trailerTemp;
                }
            });

            // film sugestion
            document('a.bxitem-link').each(function(i,e){

                var urlFilm = document(this).attr('href');
                var sugestionUUID = getUUIDFormURL(urlFilm);
                var filmName = ''
                var filmPoster = ''
                var spanTag = document(this).children().eq(0);
                filmName = spanTag.next().text();
                filmPoster = spanTag.children().eq(0).attr('src')

                 var metaData = {"uuid":sugestionUUID,
                        "url":urlFilm,
                        "name":filmName,
                        "poster":filmPoster
                       }
                 filmSugestionsFilm.push(metaData);

            });

            var filmMetaData = {"uuid":filmUUID,
                                "url":url,
                                "name":filmName,
                                "description":filmDescription,
                                "type":filmType,
                                "time":filmTime,
                                "year":filmYear,
                                "imdb":filmIMDBScore,
                                "country":filmcountry,
                                "nameEng":filmEngName,
                                "director":filmDriector,
                                "trailer":filmTrailer,
                                "sugestions":filmSugestionsFilm,
                                "poster":_poster
            }
            console.log(filmMetaData);
            Parse.Cloud.run("onSaveFilm",filmMetaData,{success: function(message) {
                console.log(message);
                done();
            }, error: function(error) {
                console.log(error.message);
                done();
            } });
            

            
        }

    });   
}



/*
    metadata ==> uuid , poster , banner , filmName , url
*/

function updateNewFilmBanner(metaData , done){
    Parse.Cloud.run("onSaveFilmBanner",metaData,{success: function(f) {
        console.log(f);
        done();
    }, error: function(error) {
        console.log(error);
        done();
    }});
}
function updateNewFilmHot(metaData , done){
    Parse.Cloud.run("onSaveFilmHot",metaData,{success: function(f) {
        console.log(f);
        done();
    }, error: function(error) {
        console.log(error);
        done();
    }});
}
function updateNewFilmSugestion(metaData , done){
    Parse.Cloud.run("onSaveFilmSugestion",metaData,{success: function(f) {
        console.log(f);
        done();
    }, error: function(error) {
        done();
    }});
}
function updateNewFilmMovie(metaData , done){
    Parse.Cloud.run("onSaveFilmMovie",metaData,{success: function(f) {
        console.log(f);
        done();
    }, error: function(error) {
        done();
    }});
}
function updateNewFilmDrama(metaData , done){
    Parse.Cloud.run("onSaveFilmDrama",metaData,{success: function(f) {
        console.log(f);
        done();
    }, error: function(error) {
        console.log(error);
        done();
    }});
}
function updateNewFilmCinema(metaData , done){
    Parse.Cloud.run("onSaveFilmCinema",metaData,{success: function(f) {
        console.log(f);
        done();
    }, error: function(error) {
        console.log(error);
        done();
    }});
}

function getUUIDFormURL(url){
    var uuid = "0"
    var partsOfStr = url.split("-");
    var temu = partsOfStr.pop();
    var temu2 = temu.split(".");
    var uuidtemp  = temu2[0];
    var matches = uuidtemp.match(/\d+/g);
    if (matches != null) {
        uuid = uuidtemp
    }
    return uuid
}

function startClone(){
    console.log("Server Start");
    Parse.initialize("6S9Mm3YcEJ8acOE1MAyErNPiykGJG5FWus9dfvlG","e5ovlMLyR7sLX6xJNVLf66sJ5vO9bFKQ8ZKcJY9l")
    request({'url':'http://hdonline.vn','proxy':'http://112.213.94.36:3128'}, function (error, response, html) {
    if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);
        $('div.bannersld-info').each(function(i, element){
            var a = $(this).prev();
            var filmURL = a.attr('href');            
            var uuid = getUUIDFormURL(filmURL);           
            var filmBannerImage = a.children().eq(0).attr('data-cfsrc');
            if(filmBannerImage == undefined){
                filmBannerImage = a.children().eq(0).attr('src');
            }
            var filmName = $(this).children().eq(0).text().split("  ")[0]
            var filmPoster = $(this).parent().parent().parent().children().eq(1).children().eq(i).children().eq(0).attr('data-cfsrc')
            if(filmPoster == undefined){
                filmPoster = $(this).parent().parent().parent().children().eq(1).children().eq(i).children().eq(0).attr('src')
            }
            console.log(filmPoster)
            console.log(filmBannerImage)
            var metaData = {"uuid":uuid,
                            "name":filmName,
                            "posterImage":filmPoster,
                            "url":filmURL,
                            "bannerImage":filmBannerImage}
            listFilmUpdate.push(metaData);
            listNewBanner.push(metaData);
            });
        $('div.tn-main').each(function(f, element){
            var listSection = $(this).children();
            for(var i=0;i<listSection.length;i++){
                var node = listSection.eq(i)
                if(node.attr('id') == 'scrollPhimTop'){
                    var listFilm = node.children().eq(1).children().eq(0).children().eq(0).children()
                    for(var j=0;j<listFilm.length;j++){
                        var nodeFilm = listFilm.eq(j);
                        var film = nodeFilm.children().eq(0).children().eq(0);
                        var filmLink = film.attr('href');
                        var filmPoster = ''
                        var filmVIEName = ''
                        var filmENGName = ''
                        var filmIMDBScore = '0'
                        var filmYear = '' 
                        var filmC = film.children();
                        for(var k = 0;k<filmC.length;k++){
                            if('bxitem-img' == filmC.eq(k).attr('class')){
                                filmPoster = filmC.eq(k).children().eq(0).attr('src');
                                if(filmPoster == undefined){
                                    filmPoster = filmC.eq(k).children().eq(0).attr('data-cfsrc');
                                }
                            }
                        }
                        for(var k = 0;k<film.next().children().length;k++){
                            if('name-vi' == film.next().children().eq(k).attr('class')){
                                filmENGName = film.next().children().eq(k).text();
                            }
                            else if('name-en' == film.next().children().eq(k).attr('class')){
                                filmVIEName = film.next().children().eq(k).text();
                            }
                            // 
                            else if(film.next().children().eq(k).text().indexOf('Đánh giá:') > -1){
                                filmIMDBScore = film.next().children().eq(k).text().replace('Đánh giá:','').replace(/^\s+|\s+$/g, '')
                            }
                            else if(film.next().children().eq(k).text().indexOf('Năm sản xuất:') > -1){
                                filmYear = film.next().children().eq(k).text().replace('Năm sản xuất:','').replace(/^\s+|\s+$/g, '')
                            }                            
                        }
                        
                        
                        var uuid = getUUIDFormURL(filmLink);
                        var metaData = {"uuid":uuid,
                            "url":filmLink,
                            "name":filmVIEName,
                            "nameEng":filmENGName,
                            "year":filmYear,
                            "imdb":filmIMDBScore,
                            "posterImage":filmPoster
                        }
                        listFilmUpdate.push(metaData);
                        listFilmHot.push(metaData);
                        
                    }
                }
                else if(node.attr('id') == 'scrollPhimHotHome'){
                    var listFilm = node.children().eq(1).children().eq(0).children().eq(0).children()
                    for(var j=0;j<listFilm.length;j++){
                        var nodeFilm = listFilm.eq(j);
                        var film = nodeFilm.children().eq(0).children().eq(0);
                        var filmLink = film.attr('href');
                        var filmPoster = ''
                        var filmVIEName = ''
                        var filmENGName = ''
                        var filmIMDBScore = '0'
                        var filmYear = ''
                        var filmC = film.children();
                        for(var k = 0;k<filmC.length;k++){
                            if('bxitem-img' == filmC.eq(k).attr('class')){
                                filmPoster = filmC.eq(k).children().eq(0).attr('src');
                                if(filmPoster == undefined){
                                    filmPoster = filmC.eq(k).children().eq(0).attr('data-cfsrc');
                                }
                            }
                        }
                        for(var k = 0;k<film.next().children().length;k++){
                            if('name-vi' == film.next().children().eq(k).attr('class')){
                                filmENGName = film.next().children().eq(k).text();
                            }
                            else if('name-en' == film.next().children().eq(k).attr('class')){
                                filmVIEName = film.next().children().eq(k).text();
                            }
                            else if(film.next().children().eq(k).text().indexOf('Đánh giá:') > -1){
                                filmIMDBScore = film.next().children().eq(k).text().replace('Đánh giá:','').replace(/^\s+|\s+$/g, '')
                            }
                            else if(film.next().children().eq(k).text().indexOf('Năm sản xuất:') > -1){
                                filmYear = film.next().children().eq(k).text().replace('Năm sản xuất:','').replace(/^\s+|\s+$/g, '')
                            }
                        }
                        var uuid = getUUIDFormURL(filmLink);
                        
                        var metaData = {"uuid":uuid,
                            "url":filmLink,
                            "name":filmVIEName,
                            "nameEng":filmENGName,
                            "year":filmYear,
                            "imdb":filmIMDBScore,
                            "posterImage":filmPoster
                        }
                        listFilmUpdate.push(metaData);
                        listFilmSuggestion.push(metaData);
                        //Parse.Cloud.run("onSaveFilmSuggestion",metaData,{success: function(ratings) {}, error: function(error) { } });
                    }
                }
                else if(node.attr('id') == 'scrollPhimLe'){
                    var listFilm = node.children().eq(1).children().eq(0).children().eq(0).children()
                    for(var j=0;j<listFilm.length;j++){
                        var nodeFilm = listFilm.eq(j);
                        var film = nodeFilm.children().eq(0).children().eq(0);
                        var filmLink = film.attr('href');
                        var filmPoster = ''
                        var filmVIEName = ''
                        var filmENGName = ''
                        var filmIMDBScore = '0'
                        var filmYear = ''
                        var filmC = film.children();
                        for(var k = 0;k<filmC.length;k++){
                            if('bxitem-img' == filmC.eq(k).attr('class')){
                                filmPoster = filmC.eq(k).children().eq(0).attr('src');
                                if(filmPoster == undefined){
                                    filmPoster = filmC.eq(k).children().eq(0).attr('data-cfsrc');
                                }
                            }
                        }
                        for(var k = 0;k<film.next().children().length;k++){
                            if('name-vi' == film.next().children().eq(k).attr('class')){
                                filmENGName = film.next().children().eq(k).text();
                            }
                            else if('name-en' == film.next().children().eq(k).attr('class')){
                                filmVIEName = film.next().children().eq(k).text();
                            }
                            else if(film.next().children().eq(k).text().indexOf('Đánh giá:') > -1){
                                filmIMDBScore = film.next().children().eq(k).text().replace('Đánh giá:','').replace(/^\s+|\s+$/g, '')
                            }
                            else if(film.next().children().eq(k).text().indexOf('Năm sản xuất:') > -1){
                                filmYear = film.next().children().eq(k).text().replace('Năm sản xuất:','').replace(/^\s+|\s+$/g, '')
                            }
                        }
                        var uuid = getUUIDFormURL(filmLink);
                        
                        var metaData = {"uuid":uuid,
                            "url":filmLink,
                            "name":filmVIEName,
                            "nameEng":filmENGName,
                            "year":filmYear,
                            "imdb":filmIMDBScore,
                            "posterImage":filmPoster
                        }
                        listFilmUpdate.push(metaData);
                        listMovieNew.push(metaData);
                        //Parse.Cloud.run("onSaveFilmMovieNew",metaData,{success: function(ratings) {}, error: function(error) { } });
                    }
                }
                else if(node.attr('id') == 'scrollPhimBo'){
                    var listFilm = node.children().eq(1).children().eq(0).children().eq(0).children()
                    for(var j=0;j<listFilm.length;j++){
                        var nodeFilm = listFilm.eq(j);
                        var film = nodeFilm.children().eq(0).children().eq(0);
                        var filmLink = film.attr('href');
                        var filmPoster = ''
                        var filmVIEName = ''
                        var filmENGName = ''
                        var filmIMDBScore = '0'
                        var filmYear = ''
                        var filmC = film.children();
                        for(var k = 0;k<filmC.length;k++){
                            if('bxitem-img' == filmC.eq(k).attr('class')){
                                filmPoster = filmC.eq(k).children().eq(0).attr('src');
                                if(filmPoster == undefined){
                                    filmPoster = filmC.eq(k).children().eq(0).attr('data-cfsrc');
                                }
                            }
                        }
                        for(var k = 0;k<film.next().children().length;k++){
                            if('name-vi' == film.next().children().eq(k).attr('class')){
                                filmENGName = film.next().children().eq(k).text();
                            }
                            else if('name-en' == film.next().children().eq(k).attr('class')){
                                filmVIEName = film.next().children().eq(k).text();
                            }
                            else if(film.next().children().eq(k).text().indexOf('Đánh giá:') > -1){
                                filmIMDBScore = film.next().children().eq(k).text().replace('Đánh giá:','').replace(/^\s+|\s+$/g, '')
                            }
                            else if(film.next().children().eq(k).text().indexOf('Năm sản xuất:') > -1){
                                filmYear = film.next().children().eq(k).text().replace('Năm sản xuất:','').replace(/^\s+|\s+$/g, '')
                            }
                        }
                        var uuid = getUUIDFormURL(filmLink);
                        
                        var metaData = {"uuid":uuid,
                            "url":filmLink,
                            "name":filmVIEName,
                            "nameEng":filmENGName,
                            "year":filmYear,
                            "imdb":filmIMDBScore,
                            "posterImage":filmPoster
                        }
                        listFilmUpdate.push(metaData);
                        listDrameNew.push(metaData);
                        //Parse.Cloud.run("onSaveFilmDramaNew",metaData,{success: function(ratings) {}, error: function(error) { } });
                    }
                }
                else if(node.attr('id') == 'scrollPhimValentime'){
                    var listFilm = node.children().eq(1).children().eq(0).children().eq(0).children()
                    for(var j=0;j<listFilm.length;j++){
                        var nodeFilm = listFilm.eq(j);
                        var film = nodeFilm.children().eq(0).children().eq(0);
                        var filmLink = film.attr('href');
                        var filmPoster = ''
                        var filmVIEName = ''
                        var filmENGName = ''
                        var filmIMDBScore = '0'
                        var filmYear = ''
                        var filmC = film.children();
                        for(var k = 0;k<filmC.length;k++){
                            if('bxitem-img' == filmC.eq(k).attr('class')){
                                filmPoster = filmC.eq(k).children().eq(0).attr('src');
                                if(filmPoster == undefined){
                                    filmPoster = filmC.eq(k).children().eq(0).attr('data-cfsrc');
                                }
                            }
                        }
                        for(var k = 0;k<film.next().children().length;k++){
                            if('name-vi' == film.next().children().eq(k).attr('class')){
                                filmENGName = film.next().children().eq(k).text();
                            }
                            else if('name-en' == film.next().children().eq(k).attr('class')){
                                filmVIEName = film.next().children().eq(k).text();
                            }
                            else if(film.next().children().eq(k).text().indexOf('Đánh giá:') > -1){
                                filmIMDBScore = film.next().children().eq(k).text().replace('Đánh giá:','').replace(/^\s+|\s+$/g, '')
                            }
                            else if(film.next().children().eq(k).text().indexOf('Năm sản xuất:') > -1){
                                filmYear = film.next().children().eq(k).text().replace('Năm sản xuất:','').replace(/^\s+|\s+$/g, '')
                            }
                        }
                        var uuid = getUUIDFormURL(filmLink);
                        
                        var metaData = {"uuid":uuid,
                            "url":filmLink,
                            "name":filmVIEName,
                            "nameEng":filmENGName,
                            "year":filmYear,
                            "imdb":filmIMDBScore,
                            "posterImage":filmPoster
                        }
                        listFilmUpdate.push(metaData);
                        listCinemaNew.push(metaData);
                        //Parse.Cloud.run("onSaveFilmCinemaNew",metaData,{success: function(ratings) {}, error: function(error) { } });
                    }
                }
            }
        });
        
        if(listFilmUpdate.length != 0){
            console.log("BEGIN UPDATE FILM");
            updateFilm(0,listFilmUpdate);
        }
        else{
            console.log("Lỗi Parse dữ liệu");
        }
    }
        else{
            console.log("ERROR");
        }
    })
}
function updateFilm(index , list){
    if(index < list.length){
        getFilmDetailsAndSubmitToServer(list[index]['url'],list[index]['posterImage'],function (){
            updateFilm(index + 1,list);
        })
    }
    else{
        // save banner
        console.log("Begin Save Banner");
        updateBanner(0,listNewBanner);
    }
}
function updateBanner(index , list){
    if(index < list.length){
        updateNewFilmBanner(list[index],function (){
            updateBanner(index + 1,list);
        })
    }
    else{
        console.log("Begin Save Hot");
        // save film hot
        updateHotFilm(0,listFilmHot);
    }
}
function updateHotFilm(index , list){
    if(index < list.length){
        updateNewFilmHot(list[index],function (){
            updateHotFilm(index + 1,list);
        })
    }
    else{
        console.log("Begin Save Sugestion");
        updateSugestionFilm(0,listFilmSuggestion);
    }
}
function updateSugestionFilm(index , list){
    if(index < list.length){
        updateNewFilmSugestion(list[index],function (){
            updateSugestionFilm(index + 1,list);
        })
    }
    else{
        console.log("Begin Save Movie");
        updateMovieFilm(0,listMovieNew);
    }
}
function updateMovieFilm(index , list){
    if(index < list.length){
        updateNewFilmMovie(list[index],function (){
            updateMovieFilm(index + 1,list);
        })
    }
    else{
        console.log("Begin Save Drama");
        // save film hot
        updateMovieDrama(0,listDrameNew);
    }
}
function updateMovieDrama(index , list){
    if(index < list.length){
        updateNewFilmDrama(list[index],function (){
            updateMovieDrama(index + 1,list);
        })
    }
    else{
        console.log("Begin Save Cinema");
        updateMovieCinema(0,listCinemaNew);
    }
}
function updateMovieCinema(index , list){
    if(index < list.length){
        updateNewFilmCinema(list[index],function (){
            updateMovieCinema(index + 1,list);
        })
    }
    else{
        console.log("Done Sync");
    }
}
startClone();

