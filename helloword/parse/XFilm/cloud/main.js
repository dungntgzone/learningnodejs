
// Save Film Home Page
Parse.Cloud.define("onSaveFilmBanner",function(request, response){
    var uuid = request.params["uuid"]
    var Fbanner = Parse.Object.extend("Banner");
    var query = new Parse.Query(Fbanner);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var fbanner = new Fbanner();
                fbanner.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                },
                    fbanner: function(model, error) {
                        response.error("Error -> " + error);
                    }});
        }
        else if ( results.length == 1){
            result = results[0];
            result.set("name", request.params.name);
            result.set("posterImage", request.params.posterImage);
            result.set("bannerImage", request.params.bannerImage);
            result.save();
            response.success("Update Done Film -> " + request.params.name);
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});

});
// Save Film Hot
Parse.Cloud.define("onSaveFilmHot",function(request, response){
    var uuid = request.params["uuid"]
    var Fbanner = Parse.Object.extend("Hot");
    var query = new Parse.Query(Fbanner);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var fbanner = new Fbanner();
                fbanner.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                },
                    fbanner: function(model, error) {
                        response.error("Error -> " + error);
                    }});
        }
        else if ( results.length == 1){
            result = results[0];
            result.set("name", request.params.name);
            result.set("posterImage", request.params.posterImage);
            result.set("bannerImage", request.params.bannerImage);
            result.set("year", request.params.year);
            result.set("imdb", request.params.imdb);
            result.save();
            response.success("Update Done Film -> " + request.params.name);
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});

});
// save film sugestion
Parse.Cloud.define("onSaveFilmSugestion",function(request, response){
    var uuid = request.params["uuid"]
    var Fbanner = Parse.Object.extend("Sugestion");
    var query = new Parse.Query(Fbanner);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var fbanner = new Fbanner();
                fbanner.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                },
                    fbanner: function(model, error) {
                        response.error("Error -> " + error);
                    }});
        }
        else if ( results.length == 1){
            result = results[0];
            result.set("name", request.params.name);
            result.set("posterImage", request.params.posterImage);
            result.set("bannerImage", request.params.bannerImage);
            result.set("year", request.params.year);
            result.set("imdb", request.params.imdb);
            result.save();
            response.success("Update Done Film -> " + request.params.name);
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});

});
// save film Movie
Parse.Cloud.define("onSaveFilmMovie",function(request, response){
    var uuid = request.params["uuid"]
    var Fbanner = Parse.Object.extend("Movie");
    var query = new Parse.Query(Fbanner);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var fbanner = new Fbanner();
                fbanner.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                },
                    fbanner: function(model, error) {
                        response.error("Error -> " + error);
                    }});
        }
        else if ( results.length == 1){
            result = results[0];
            result.set("name", request.params.name);
            result.set("posterImage", request.params.posterImage);
            result.set("bannerImage", request.params.bannerImage);
            result.set("year", request.params.year);
            result.set("imdb", request.params.imdb);
            result.save();
            response.success("Update Done Film -> " + request.params.name);
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});

});
// save film Drama
Parse.Cloud.define("onSaveFilmDrama",function(request, response){
    var uuid = request.params["uuid"]
    var Fbanner = Parse.Object.extend("Drama");
    var query = new Parse.Query(Fbanner);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var fbanner = new Fbanner();
                fbanner.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                },
                    fbanner: function(model, error) {
                        response.error("Error -> " + error);
                    }});
        }
        else if ( results.length == 1){
            result = results[0];
            result.set("name", request.params.name);
            result.set("posterImage", request.params.posterImage);
            result.set("bannerImage", request.params.bannerImage);
            result.set("year", request.params.year);
            result.set("imdb", request.params.imdb);
            result.save();
            response.success("Update Done Film -> " + request.params.name);
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});

});
// save film Cinema
Parse.Cloud.define("onSaveFilmCinema",function(request, response){
    var uuid = request.params["uuid"]
    var Fbanner = Parse.Object.extend("Cinema");
    var query = new Parse.Query(Fbanner);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var fbanner = new Fbanner();
                fbanner.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                },
                    fbanner: function(model, error) {
                        response.error("Error -> " + error);
                    }});
        }
        else if ( results.length == 1){
            result = results[0];
            result.set("name", request.params.name);
            result.set("posterImage", request.params.posterImage);
            result.set("bannerImage", request.params.bannerImage);
            result.set("year", request.params.year);
            result.set("imdb", request.params.imdb);
            result.save();
            response.success("Update Done Film -> " + request.params.name);
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});

});

// Save All Metadata FILM
Parse.Cloud.define("onSaveFilm",function(request, response){
    console.log(request.params.name);
    var uuid = request.params.uuid
    var Film = Parse.Object.extend("Film");
    var query = new Parse.Query(Film);
    query.limit(1);
    query.equalTo("uuid",uuid);
    query.find({
    success: function(results) {
        if (results.length == 0){
                var film = new Film();            
                film.save(request.params, {success: function(object) {
                    response.success("Save Done Film -> " + request.params.name);
                }, film: function(model, error){
                    response.error("Error -> " + error);
                }});
        }
        else if ( results.length == 1){
            result = results.pop();
            result.set("name", request.params.name);
            result.set("url", request.params.url);
            result.set("description", request.params.description);
            result.set("type", request.params.type);
            result.set("time", request.params.time);
            result.set("year", request.params.year);
            result.set("imdb", request.params.imdb);
            result.set("country", request.params.country);
            result.set("nameEng", request.params.nameEng);
            result.set("director", request.params.director);
            result.set("trailer", request.params.trailer);
            result.set("sugestions", request.params.sugestions);
            result.set("poster", request.params.poster);
            result.save();
            result.save(null, {success: function(object) {
                    response.success("Update Done Film -> " + request.params.name);
                }, film: function(model, error){
                    response.error("Error -> " + error);
                }});
        }
    },
    error: function(error) {
        response.error("Error -> " + error);
    }});
});


// GET film home PAGE
Parse.Cloud.define("homePage",function(request, response){
    var Fbanner = Parse.Object.extend("Banner");
    var query = new Parse.Query(Fbanner);
    query.limit(7);
    query.descending("updatedAt");
    query.find({ success: function(results) {
        var FHot  = Parse.Object.extend("Hot");
        var queryHot = new Parse.Query(FHot);
        queryHot.limit(20);
        queryHot.descending("updatedAt");
        queryHot.find({ success: function(resultsHot) {
            var Fsugges  = Parse.Object.extend("Sugestion");
            var querySugges = new Parse.Query(Fsugges);
            querySugges.limit(20);
            querySugges.descending("updatedAt");
            querySugges.find({ success: function(resultsSugges) {
                
                var FMovie  = Parse.Object.extend("Movie");
                var queryFMovie = new Parse.Query(FMovie);
                queryFMovie.limit(20);
                queryFMovie.descending("updatedAt");
                queryFMovie.find({ success: function(resultsMovie) {
                    
                    var FDrama  = Parse.Object.extend("Drama");
                    var queryFDrama = new Parse.Query(FDrama);
                    queryFDrama.limit(20);
                    queryFDrama.descending("updatedAt");
                    queryFDrama.find({ success: function(resultsDrama) {
                        
                        var FCinema  = Parse.Object.extend("Cinema");
                        var queryFCinema = new Parse.Query(FCinema);
                        queryFCinema.limit(20);
                        queryFCinema.descending("updatedAt");
                        queryFCinema.find({ success: function(resultsCinema) {
                            response.success({"message":"success","status":"1","banner":results,"home":[{"Phim Xem nhiều":resultsHot},{"Phim đề cử":resultsSugges},{"Phim lẻ mới":resultsMovie},{"Phim bộ mới":resultsDrama},{"Phim chiếu rạp":resultsCinema}]});
                        },error: function(error) {
                            response.success({"message":"failer","status":"0"});
                        }});
                        
                    },error: function(error) {
                        response.success({"message":"failer","status":"0"});
                    }});
                    
                },error: function(error) {
                    response.success({"message":"failer","status":"0"});
                }});
                
                
                
            },error: function(error) {
                response.success({"message":"failer","status":"0"});
            }});
        },error: function(error) {
            response.success({"message":"failer","status":"0"});
        }});
        
    },
    error: function(error) {
        response.success({"message":"failer","status":"0"});
    }});
});
