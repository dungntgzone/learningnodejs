var Parse = require("Parse").Parse;
var request = require('request');
var requestsync = require('sync-request');
var cheerio = require('cheerio');
var listFilm = []
function getFilmDetailsAndSubmitToServer(url,done){
    console.log('http://hdonline.vn' + url);
    request('http://hdonline.vn' + url, function (error, response, html) {
        if (!error && response.statusCode == 200) {


            var document = cheerio.load(html);
            var filmUUID = '0'
            var filmName = ''
            var filmEngName = ''
            var filmType = ''
            var filmcountry = ''
            var filmTime = ''
            var filmYear = ''
            var filmDriector = ''
            var filmIMDBScore = ''
            var filmDescription = ''
            var filmTrailer = ''
            var filmSugestionsFilm = []
            var filmScreenshot = []

            var partsOfStr = url.split("-");
            var temu = partsOfStr.pop();
            var temu2 = temu.split(".");
            var uuidtemp  = temu2[0];
            var matches = uuidtemp.match(/\d+/g);
            if (matches != null) {
                filmUUID = uuidtemp
            }


            document('ul.filminfo-fields').each(function(i,ele){
                var listInfo = document(this).children();
                for(var j=0;j<listInfo.length;j++){
                    var litag = listInfo.eq(j);

                    //console.log(litag.children().eq(0).attr());
                    var LI_content = litag.text();

                    if(LI_content.indexOf('Tên Tiếng Anh:') > -1){
                        filmEngName = LI_content;
                    }
                    else if(LI_content.indexOf('Tên Phim:') > -1){
                        filmName = LI_content;
                    }
                    else if(LI_content.indexOf('Thể loại:') > -1){
                        filmType = LI_content;
                    }
                    else if(LI_content.indexOf('Quốc gia:') > -1){
                        filmcountry = LI_content;
                    }
                    else if(LI_content.indexOf('Thời lượng:') > -1){
                        filmTime = LI_content;
                    }
                    else if(LI_content.indexOf('Năm sản xuất:') > -1){
                        filmYear = LI_content;
                    }
                    else if(LI_content.indexOf('Đạo diễn:') > -1){
                        filmDriector = LI_content
                    }
                    //
                }


            });
            // film des
            document('div.rating').each(function(i,e){
                filmDescription = document(this).next().children().eq(0).text();
            });
            // trailer
            document('div.header-block-title ').each(function(i,e){
                var urlTrailer = document(this).next().attr('src');
                if(urlTrailer != undefined){
                    var trailerTemp = urlTrailer.split("=").pop();
                    filmTrailer = trailerTemp;
                }
            });

            // film sugestion
            document('a.bxitem-link').each(function(i,e){

                var urlFilm = document(this).attr('href');
                var sugestionUUID = '0'
                var uuid = '0'
                var filmName = ''
                var filmPoster = ''
                var partsOfStr = urlFilm.split("-");
                var temu = partsOfStr.pop();
                var temu2 = temu.split(".");
                var uuidtemp  = temu2[0];
                var matches = uuidtemp.match(/\d+/g);
                if (matches != null) {
                    sugestionUUID = uuidtemp
                }

                var spanTag = document(this).children().eq(0);
                filmName = spanTag.next().text();
                filmPoster = spanTag.children().eq(0).attr('data-cfsrc')

                 var metaData = {"uuid":sugestionUUID,
                        "url":urlFilm,
                        "name":filmName,
                        "filmPoster":filmPoster
                       }
                 filmSugestionsFilm.push(metaData);

            });
            document('img.screenshot-slide-popup').each(function(i,e){
                var src = document(this).attr('data-cfsrc');
                if(src != undefined){
                    filmScreenshot.push(src.replace("//","/"));
                }
            });

            var filmMetaData = {"uuid":filmUUID,
                                "url":url,
                                "name":filmName,
                                "description":filmDescription,
                                "type":filmType,
                                "time":filmTime,
                                "year":filmYear,
                                "imdb":filmIMDBScore,
                                "bannerImage":'',
                                "engName":filmEngName,
                                "director":filmDriector,
                                "tailer":filmTrailer,
                                "sugestion":filmSugestionsFilm,
                                "screenshot":filmScreenshot
            }
            console.log(filmMetaData);
            Parse.Cloud.run("onSaveFilm",filmMetaData,{success: function(ratings) {}, error: function(error) { } });
            done();

            
        }

    });   
}

function loadGroup(url){
    console.log(url);
    request(url, function (error, response, html) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html);
            
            
            // lay danh sach film
            var listFilm = $('div.tn-bxitem');
            for(var i = 0;i<listFilm.length ; i++){
                var nodeF = listFilm.eq(i);
                var atag =  nodeF.children().eq(0);
//                getFilmDetailsAndSubmitToServer(atag.attr('href'),function(){
//                    
//                });
            }
            // trang tiep
            $('ul.pagination').each(function(i,element){
                var hasNext = false
                var nextURL = ""
                for(var j = 0;j<$(this).children().length;j++){
                    var nodePage = $(this).children().eq(j);
                    if('Trang Sau' == nodePage.text()){
                        hasNext = true;
                        nextURL = nodePage.children().eq(0).attr('href');
                    }
                }
                if(hasNext){
                    if(nextURL.indexOf('http') > -1){
                        loadGroup(nextURL);
                    }
                    else{
                        loadGroup('http://hdonline.vn' + nextURL);
                    }
                }
                else{
                }
            });
        }
    });
}

function grorpNext(listSUB,index){
    
    if(index < listSUB.length){
        var node = listSUB.eq(index)
        var nodeName = node.text();
        var nodeURL  =  node.children().eq(0).attr('href');
        if(typeof nodeURL === 'undefined'){
            console.log("Loi Node");
            grorpNext(listSUB,index+1);
        }
        else{
            if(nodeURL.indexOf(".html") > -1){
                
                grorpNext(listSUB,index+1);
            }
        }
    }
    else{
        console.log(index);
        console.log("Done Load FILM");
    }
}

function start(){
    console.log("Server Start");
    Parse.initialize("tdQbpvVMXWR5z878P0OrRTGuGXn0vOUS4Ii9Ribz","00l9y4tgayg9UqXScCCINIe3OSrtDPBJWIp8iPXT");
    request('http://hdonline.vn/', function (error, response, html) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html);
            var tngnavsub = $('div.tn-gnavsub')
            for(var i=0;i<tngnavsub.length;i++){
                var nodeNAV = tngnavsub.eq(i);
                var listSUB = nodeNAV.children().eq(0).children().eq(0).children();
                for(var j = 0;j<listSUB.length;j++){
                    
                    var node = listSUB.eq(j)
                    var nodeName = node.text();
                    var nodeURL  =  node.children().eq(0).attr('href');
                    
                    if(typeof nodeURL !=  undefined){
                        if(nodeURL.indexOf(".html") > -1){
                            if(nodeURL.indexOf('http') > -1){
                                loadGroup(nodeURL);
                            }
                            else{
                                loadGroup('http://hdonline.vn' + nodeURL);
                            }
                            
                        }
                    }
                }
            }
            console.log("Done Load Page");
        }
    });
    
}
start();
//getFilmDetailsAndSubmitToServer("http://hdonline.vn/phim-tan-tieu-ngao-giang-ho-3132.html");
